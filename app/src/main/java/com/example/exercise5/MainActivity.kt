package com.example.exercise5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import com.example.exercise5.databinding.ActivityMainBinding
import com.google.android.material.snackbar.Snackbar


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        init()
    }

    fun init() {
        binding.signInBtn.setOnClickListener {
            saveInfo()
        }
    }

    fun saveInfo() {
        val email = binding.email.text.toString()
        val userName = binding.userName.text.toString()
        val intent = Intent(this, MainActivity2::class.java)
        intent.putExtra("userName", userName)

        if (email.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(email).matches())
            startActivity(intent)
        else Toast.makeText(this, "email is not valid", Toast.LENGTH_SHORT).show()


    }
}