package com.example.exercise5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.exercise5.databinding.ActivityMain2Binding

class MainActivity2 : AppCompatActivity() {
    private lateinit var binding: ActivityMain2Binding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMain2Binding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        displayData()
    }

    fun displayData() {
        val text = intent.extras?.getString("userName", "")
        binding.textView.text =  "Welcome ${text}!"
    }
}